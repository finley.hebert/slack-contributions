#!/bin/bash
declare -A userCount
count=0
max=370000
for directory in output/*; do
    if [ -d "$directory" ]; then
        for file in $directory/*.json; do
            for user in $(cat $file | jq --raw-output ".[] | .user"); do
                ((count++))
            let '++userCount[$user]'
            done
        done
    fi
done

for i in "${!userCount[@]}"
do
    echo "User: $i, Messages: ${userCount[$i]}"
done
